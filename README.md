This is the MATLAB-class to work with Novoptel EPS1000 device by Ethernet (USB connection isn't support in this version).
It was made from scripts of Novoptel for ease of using (only one file needed)

First, you create a class:
ip = '10.28.18.3'; % ip of the EPS1000 device 
N = Eps1000(ip); % make an object to work

Now we are able to work with the device by using methods:
N.setStausQWP1('forward'); % status of the channel (disabled, forward or backward)
N.setValueQWP1(105.43); % value of the channel

Same for the getting process:
currentStatusQWP2 = N.statusQWP2(); % or N.status('QWP2');
currentValueQWP2 = N.valueQWP2(); % or N.value('QWP2')