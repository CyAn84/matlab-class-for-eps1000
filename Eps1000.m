classdef Eps1000
% Novoptel EPS1000 Endless Polarization Scrambler class
    
    properties (Access = protected)
        TCPHandle;
    end
    
    properties
        manufacturer = 'Novoptel';
        model = 'EPS1000';
        ip;
    end
    
    methods (Access = protected)
      
        function [res, ok, BytesAvailable] = readtcp(obj, addr)

            debugmode = 0;
            ok = 0;

            if length(addr) == 1
                seq1 = [ 82 bitshift(addr, -8) mod(addr, 2^8)];
            else
                seq1 = [ 77 length(addr) ];
                for ii=1:length(addr)
                    seq1 = [ seq1 bitshift(addr(ii), -8) mod(addr(ii), 2^8)];
                end
            end

            fopen(obj.TCPHandle);
            fwrite(obj.TCPHandle, seq1);
             
            %pause(.5);

            BytesAvailable = 0;
            StopCounter = 0;
            while BytesAvailable < 2*length(addr) & StopCounter < 10000
                BytesAvailable = obj.TCPHandle.BytesAvailable;
                StopCounter = StopCounter + 1;
            end

            res = 0;
            if BytesAvailable >= 2*length(addr)
                A = fread(obj.TCPHandle, BytesAvailable);
                %A
                for ii=1:length(addr)
                    res(ii) = A(2*(ii-1)+1)*2^8 + A(2*(ii-1)+2);
                    ok = 1;
                end
            else
                BytesAvailable;
                StopCounter;
            end

            fclose(obj.TCPHandle);
                       
            %BytesAvailable
            %TCPHandle
    
            %pause;
    
            % debug2:
            %readepc(512+426)
        
        end % readtcp
        
        function [ok] = writetcp(obj, addr, data)

            debugmode = 0;
            ok = 0;

            seq1 = [  87 bitshift(addr, -8) mod(addr, 2^8) bitshift(data, -8) mod(data, 2^8)];
    
            fopen(obj.TCPHandle);
            fwrite(obj.TCPHandle, seq1);
            fclose(obj.TCPHandle);
            ok = 1;

        end % writetcp
      
    end % protected methods
    
    methods
      
        function obj = Eps1000(address)
            
            if nargin == 0
                address = '10.40.16.51'; % Change for default ip address
            end
            
            obj.ip = address;
            obj.TCPHandle = tcpip(address, 5025);
            
            % Set size of receiving buffer, if needed. 
            set(obj.TCPHandle, 'InputBufferSize', 8192);
            
            % fopen(obj.TCPHandle);
            
        end
        
        function delete(obj)
          
            % Disconnect and clean up the server connection. 
            % fclose(obj.TCPHandle); % Moved to read and write private methods
         
        end
        
        function status = status(obj, arg)
        % Returns the status of the chosen channel
        % Channels: 'HWP' | 'QWP0' | 'QWP1' | 'QWP2' | 'QWP3' | 'QWP4' | 'QWP5'

            switch arg
                case {'HWP', 'hwp'}
                    regAddr = 0;
                case {'QWP0', 'qwp0'}
                    regAddr = 1;
                case {'QWP1', 'qwp1'}
                    regAddr = 2;
                case {'QWP2', 'qwp2'}
                    regAddr = 3;
                case {'QWP3', 'qwp3'}
                    regAddr = 4;
                case {'QWP4', 'qwp4'}
                    regAddr = 5;
                case {'QWP5', 'qwp5'}
                    regAddr = 6;
                otherwise
                    disp('Wrong parameter!');
                    return;
            end

            resp = obj.readtcp(regAddr);

            switch resp
                case {0, 2}
                    status = 'Disabled';
                case 1
                    status = 'Forward';
                case 3
                    status = 'Backward';
                otherwise
                    status = 'Error!';
            end
        end % status
      
        % [METHODS FOR EASY CONTROL THE CHANNELS]
        
        function status = statusHWP(obj)
            status = obj.status('HWP');
        end
        
        function status = statusQWP0(obj)
            status = obj.status('QWP0');
        end
        
        function status = statusQWP1(obj)
            status = obj.status('QWP1');
        end
        
        function status = statusQWP2(obj)
            status = obj.status('QWP2');
        end
        
        function status = statusQWP3(obj)
            status = obj.status('QWP3');
        end
        
        function status = statusQWP4(obj)
            status = obj.status('QWP4');
        end
        
        function status = statusQWP5(obj)
            status = obj.status('QWP5');
        end
        
        function setStatus(obj, channel, status)
        % Sets the chosen channel in condition: 'Forward' | 'Backward' | 'Disabled'
        % Channels: 'HWP' | 'QWP0' | 'QWP1' | 'QWP2' | 'QWP3' | 'QWP4' | 'QWP5'

            switch channel
                case {'HWP', 'hwp'}
                    regAddr = 0;
                case {'QWP0', 'qwp0'}
                    regAddr = 1;
                case {'QWP1', 'qwp1'}
                    regAddr = 2;
                case {'QWP2', 'qwp2'}
                    regAddr = 3;
                case {'QWP3', 'qwp3'}
                    regAddr = 4;
                case {'QWP4', 'qwp4'}
                    regAddr = 5;
                case {'QWP5', 'qwp5'}
                    regAddr = 6;
                otherwise
                    disp('Wrong channel!');
                    return;
            end

            switch status
                case {'Disabled', 'D', 'd'}
                    status = 0;
                case {'Forward', 'F', 'f'}
                    status = 1;
                case {'Backward', 'B', 'b'}
                    status = 3;
                otherwise
                    disp('Wrong status!');
                    return;
            end
            
            obj.writetcp(regAddr, status);
            
        end % setStatus
        
        % [METHODS FOR EASY CONTROL THE CHANNELS]
        
        function setStatusHWP(obj, status)
            obj.setStatus('HWP', status);
        end
        
        function setStatusQWP0(obj, status)
            obj.setStatus('QWP0', status);
        end
        
        function setStatusQWP1(obj, status)
            obj.setStatus('QWP1', status);
        end
        
        function setStatusQWP2(obj, status)
            obj.setStatus('QWP2', status);
        end
        
        function setStatusQWP3(obj, status)
            obj.setStatus('QWP3', status);
        end
        
        function setStatusQWP4(obj, status)
            obj.setStatus('QWP4', status);
        end
        
        function setStatusQWP5(obj, status)
            obj.setStatus('QWP5', status);
        end
        
        function [out, ok] = value(obj, channel)
        % Returns the value of the chosen channel
        % Channels: 'HWP' | 'QWP0' | 'QWP1' | 'QWP2' | 'QWP3' | 'QWP4' | 'QWP5'

            switch channel
                case {'HWP', 'hwp'}
                    regLoAddr = 9;
                    regHiAddr = 10;
                case {'QWP0', 'qwp0'}
                    regLoAddr = 11;
                    regHiAddr = 12;
                case {'QWP1', 'qwp1'}
                    regLoAddr = 13;
                    regHiAddr = 14;
                case {'QWP2', 'qwp2'}
                    regLoAddr = 15;
                    regHiAddr = 16;
                case {'QWP3', 'qwp3'}
                    regLoAddr = 17;
                    regHiAddr = 18;
                case {'QWP4', 'qwp4'}
                    regLoAddr = 19;
                    regHiAddr = 20;
                case {'QWP5', 'qwp5'}
                    regLoAddr = 21;
                    regHiAddr = 22;
                otherwise
                    disp('Wrong channel!');
                    ok = 0;
                    return;
            end
            
            lo = obj.readtcp(regLoAddr);
            bin16Lo = dec2bin(lo);
            if length(bin16Lo) < 16
               for i = 1:(16 - length(bin16Lo))
                   bin16Lo = ['0' bin16Lo];
               end
            end

            hi = obj.readtcp(regHiAddr);
            bin16Hi = dec2bin(hi);
            out = bin2dec([bin16Hi bin16Lo])/100;
            ok = 1;
        end % value
        
        % [METHODS FOR EASY CONTROL THE CHANNELS]
        
        function [out, ok] = valueHWP(obj)
            [out, ok] = obj.value('HWP');
        end
        
        function [out, ok] = valueQWP0(obj)
            [out, ok] = obj.value('QWP0');
        end

        function [out, ok] = valueQWP1(obj)
            [out, ok] = obj.value('QWP1');
        end
        
        function [out, ok] = valueQWP2(obj)
            [out, ok] = obj.value('QWP2');
        end
        
        function [out, ok] = valueQWP3(obj)
            [out, ok] = obj.value('QWP3');
        end
        
        function [out, ok] = valueQWP4(obj)
            [out, ok] = obj.value('QWP4');
        end
        
        function [out, ok] = valueQWP5(obj)
            [out, ok] = obj.value('QWP5');
        end
 
        
        function setValue(obj, channel, val)
        % Sets the value of the chosen channel
        % Channels: 'HWP' | 'QWP0' | 'QWP1' | 'QWP2' | 'QWP3' | 'QWP4' | 'QWP5'

            switch channel
                case {'HWP', 'hwp'}
                    regLoAddr = 9;
                    regHiAddr = 10;
                case {'QWP0', 'qwp0'}
                    regLoAddr = 11;
                    regHiAddr = 12;
                case {'QWP1', 'qwp1'}
                    regLoAddr = 13;
                    regHiAddr = 14;
                case {'QWP2', 'qwp2'}
                    regLoAddr = 15;
                    regHiAddr = 16;
                case {'QWP3', 'qwp3'}
                    regLoAddr = 17;
                    regHiAddr = 18;
                case {'QWP4', 'qwp4'}
                    regLoAddr = 19;
                    regHiAddr = 20;
                case {'QWP5', 'qwp5'}
                    regLoAddr = 21;
                    regHiAddr = 22;
                otherwise
                    disp('Wrong channel!');
                    return;
            end

            valbin = dec2bin(round(val,2)*100);
            reg32 = valbin;
            for i = 1:(32 - length(valbin))
                reg32 = strcat('0', reg32);
            end
            reg16Lo = bin2dec(reg32(17:end));
            reg16Hi = bin2dec(reg32(1:16));

            obj.writetcp(regLoAddr, reg16Lo);

            obj.writetcp(regHiAddr, reg16Hi);

        end
        
        % [METHODS FOR EASY CONTROL THE CHANNELS]
        
        function setValueHWP(obj, val)
            obj.setValue('HWP', val);
        end
        
        function setValueQWP0(obj, val)
            obj.setValue('QWP0', val);
        end

        function setValueQWP1(obj, val)
            obj.setValue('QWP1', val);
        end

        function setValueQWP2(obj, val)
            obj.setValue('QWP2', val);
        end
        
        function setValueQWP3(obj, val)
            obj.setValue('QWP3', val);
        end
        
        function setValueQWP4(obj, val)
            obj.setValue('QWP4', val);
        end
        
        function setValueQWP5(obj, val)
            obj.setValue('QWP5', val);
        end
        
    end
end
